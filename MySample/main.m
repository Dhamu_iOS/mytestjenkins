//
//  main.m
//  MySample
//
//  Created by iOSDev on 01/03/18.
//  Copyright © 2018 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
