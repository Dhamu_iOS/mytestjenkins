//
//  AppDelegate.h
//  MySample
//
//  Created by iOSDev on 01/03/18.
//  Copyright © 2018 iOSDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

