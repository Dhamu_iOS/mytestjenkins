#!/bin/sh

#  build.sh
#  MySample
#
#  Created by iOSDev on 01/03/18.
#  Copyright © 2018 iOSDev. All rights reserved.
xcodebuild -scheme MySample clean archive -archivePath build/MySample
xcodebuild -exportArchive -exportFormat ipa -archivePath "build/MySample.xcarchive" -exportPath "build/MySample.ipa" -exportProvisioningProfile "LS Store Demo"